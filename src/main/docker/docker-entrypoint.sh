#!/bin/sh

until nc -z -v -w30 $MYSQL_HOST 3306
do
  echo "Waiting for database connection..."
  sleep 2
done

java -jar /var/www/app.jar